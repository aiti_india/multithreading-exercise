package com.example.multithreadingexercise;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.widget.TextView;


public class IPFetcher extends AsyncTask<URL, String, String> {
	protected MainActivity mActivity;
	
	public IPFetcher(MainActivity a) {
		this.mActivity = a;
	}

	protected String doInBackground(URL... arg0) {
		HttpURLConnection c;
		InputStream is;
		try {
			c = (HttpURLConnection) arg0[0].openConnection();
			is = c.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		String value = null;
		try {
			BufferedReader reader =
					new BufferedReader(
							new InputStreamReader(is));
			String s;
			StringBuffer sb = new StringBuffer();
			while ((s = reader.readLine()) != null) {
				sb.append(s);
			}
			value = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			c.disconnect();
		}
		return value;
	}
	
	protected void onPostExecute(String result) {
		TextView t = (TextView) this.mActivity.findViewById(R.id.textView1);
		t.setText(result);
	}

}
